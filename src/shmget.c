/**
 * @file shmget.c
 * @author Mitsutoshi Nakano <ItSANgo@gmail.com>
 * @brief Get System V IPC shared memory.
 * @version 0.1.0
 * @date 2020-01-02
 *
 * @copyright Copyright (c) 2020 Mitsutoshi Nakano
 * 
 */

#include "shmget.h"

#include <err.h>
#include <sysexits.h>
#include <getopt.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>

int
main(int argc, char *argv[])
{
    int create_mode = 0;
    const char *key = NULL;
    const char *mode = NULL;
    for (;;) {
        static struct option options[] = {
            { "create", 0, NULL, 'c' },
            { "help", 0, NULL, 'h' },
            { "key", 1, NULL, 'k' },
            { "mode", 1, NULL, 'm' },
            { NULL, 0, NULL, 0 }
        };
        int option_index = 0;
        int c = getopt_long(argc, argv, "chk:m:", options, &option_index);
        if (c == -1) {
            break;
        }
        switch (c) {
        case 'c':
            if (create_mode) {
                warnx("can't duplicate --create\n");
                help(EX_USAGE, stderr, argv[0]); /*NOTREACHED*/
            }
            ++create_mode;
            break;
        case 'h':
            help(EX_OK, stdout, argv[0]); /*NOTREACHED*/
            break;
        case 'k':
            if (key) {
                warnx("can't duplicate --key %s, %s\n", key, optarg);
                help(EX_USAGE, stderr, argv[0]); /*NOTREACHED*/
            }
            key = optarg;
            break;
        case 'm':
            if (mode) {
                warnx("can't duplicate --mode %s, %s\n", mode, optarg);
                help(EX_USAGE, stderr, argv[0]); /*NOTREACHED*/
            }
            mode = optarg;
            break;
        case '?':
            warnx("unkown option:");
            help(EX_USAGE,stderr, argv[0]); /*NOTREACHED*/
            break;
        default:
            warnx("unkown option %d(%c)", c, c);
            assert(0);
            break;
        }
    }
    if (optind >= argc) {
        warnx("undefined size:");
        help(EX_USAGE, stderr, argv[0]); /*NOTREACHED*/
    }
    size_t size = strtoull(argv[optind], (char **)NULL, 0);
    key_t key_value = (key) ?  strtol(key, (char **)NULL, 0) : ftok(argv[0], 'M');
    mode_t mode_value = (mode) ? strtol(mode, (char **)NULL, 8) : 0;
    if (create_mode) {
        mode_value |= (IPC_CREAT | IPC_EXCL);
    }
    int id = shmget(key_value, size, mode_value);
    if (id < 0) {
        err(EX_UNAVAILABLE, "shmget():"); /*NOTREACHED*/
    }
    if (printf("%d\n", id) < 1) {
        err(EX_IOERR, "printf()"); /*NOTREACHED*/
    }
    exit(EX_OK); /*NOTREACHED*/
}
