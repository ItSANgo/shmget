#ifndef SHMGET_H
#define SHMGET_H

/**
 * @file shmget.h
 * @author Mitsutoshi Nakano <ItSANgo@gmail.com>
 * @brief Get System V IPC shared memory.
 * @version 0.1.0
 * @date 2020-01-04
 *
 * @copyright Copyright (c) 2020 Mitsutoshi Nakano
 * 
 */

#include <stdio.h>

extern void help(int exit_status, FILE *fp, const char command_name[]);

#endif /* SHMGET_H.  */
