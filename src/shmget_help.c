/**
 * @file shmget_help.c
 * @author Mitsutoshi Nakano <ItSANgo@gmail.com>
 * @brief Get System V IPC shared memory - help.
 * @version 0.1.0
 * @date 2020-01-04
 *
 * @copyright Copyright (c) 2020 Mitsutoshi Nakano
 * 
 */

#include "shmget.h"

#include <stdlib.h>
#include <stdio.h>

void
help(int exit_status, FILE *fp, const char command_name[])
{
    fprintf(fp, "usnage: %s [--help][--key key_value][--mode mode_value][--create] size\n", command_name);
    exit(exit_status); /*NOTREACHED*/
}
